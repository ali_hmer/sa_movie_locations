/* eslint-disable no-undef */
workbox.routing.registerRoute(
    // Cache CSS files
    /.*\.css/,
    // Use cache but update in the background ASAP
    workbox.strategies.staleWhileRevalidate({
        // Use a custom cache name
        cacheName: 'css-cache'
    })
);

workbox.routing.registerRoute(
    // Cache CSS files
    /.*\.(jsx?|tsx?)/,
    // Use cache but update in the background ASAP
    workbox.strategies.staleWhileRevalidate({
        // Use a custom cache name
        cacheName: 'js-cache'
    })
);

workbox.routing.registerRoute(
    // Cache image files
    /.*\.(?:png|jpg|jpeg|svg|gif)/,
    // Use the cache if it's available
    workbox.strategies.cacheFirst({
        // Use a custom cache name
        cacheName: 'image-cache',
        plugins: [
            new workbox.expiration.Plugin({
                // Cache for a maximum of a week
                maxAgeSeconds: 7 * 24 * 60 * 60,
                // Cache only 20 images
                maxEntries: 20
            })
        ]
    })
);

const bgSyncPlugin = new workbox.backgroundSync.Plugin('failedHttpQueue', {
    maxRetentionTime: 24 * 60 // Retry for max of 24 Hours
});

workbox.routing.registerRoute(
    /^https?.*/, // /api/,
    // Use the network first
    workbox.strategies.networkFirst({
        // Use a custom cache name
        cacheName: 'http-cache',
        plugins: [
            new workbox.cacheableResponse.Plugin({
                statuses: [0, 200]
                // headers: {
                //   'x-test': 'true'
                // }
            }),
            bgSyncPlugin
        ]
    })
);
