var axios = require('axios');
var httpAdapter = require('axios/lib/adapters/http');
var enzyme = require('enzyme');
var adapter = require('enzyme-adapter-react-16');

require('mutationobserver-shim');
enzyme.configure({ adapter: new adapter() });

global.shallow = enzyme.shallow;
global.render = enzyme.render;
global.mount = enzyme.mount;
global.__DEV__ = false;

axios.defaults.adapter = httpAdapter;

var localStorageMock = (function() {
    var store = {};
    return {
        clear: function() {
            store = {};
        },
        getItem: function(key) {
            return store[key];
        },
        removeItem: function(key) {
            delete store[key];
        },
        setItem: function(key, value) {
            store[key] = value.toString();
        }
    };
})();

Object.defineProperty(window, 'localStorage', { value: localStorageMock }); // eslint-disable-line no-undef
Object.defineProperty(document, 'execCommand', { value: jest.fn() }); // eslint-disable-line no-undef
Object.defineProperty(document, 'getSelection', { value: jest.fn() }); // eslint-disable-line no-undef
Object.defineProperty(window, 'print', { value: function() {} }); // eslint-disable-line no-undef
