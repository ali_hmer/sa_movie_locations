require('dotenv').config();
const path = require('path');
const compose = require('recompose/compose').default;
const webpack = require('webpack');
const withOffline = require('next-offline');
const withFonts = require('next-fonts');
const withCSS = require('@zeit/next-css');
const withTypescript = require('@zeit/next-typescript');
const OpenBrowserPlugin = require('open-browser-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const { EnvironmentPlugin } = require('webpack');

const { NODE_ENV } = process.env;
const isProd = NODE_ENV === 'production';
const NamedModulesPlugin = webpack.NamedModulesPlugin;
const NoEmitOnErrorsPlugin = webpack.NoEmitOnErrorsPlugin;
// const { alias } = require('./now.json');

const withPlugins = (nextConfig = {}) => {
    return Object.assign({}, nextConfig, {
        webpack(config, options) {
            const { dev, isServer, defaultLoaders } = options;

            if (!defaultLoaders) {
                throw new Error('This plugin is not compatible with Next.js versions below 5.0.0 https://err.sh/next-plugins/upgrade');
            }

            if (isServer) {
                config.plugins.push(
                    new ForkTsCheckerWebpackPlugin({
                        async: false,

                        checkSyntacticErrors: true,
                        tsconfig: path.resolve('./src/../tsconfig.json')
                    })
                );
            }

            config.plugins = config.plugins.filter((plugin) => {
                return plugin.constructor.name !== 'UglifyJsPlugin';
            });

            if (dev && !isServer) {
                config.plugins.push(new OpenBrowserPlugin({ url: 'http://localhost:3000' }));
            }

            if (dev) {
                config.plugins.push(new NamedModulesPlugin(), new NoEmitOnErrorsPlugin());
            }
            return config;
        }
    });
};

const withEnv = (...envKeys) => (nextConfig = {}) => {
    return Object.assign({}, nextConfig, {
        webpack(config) {
            config.plugins.push(new EnvironmentPlugin(envKeys));
            return config;
        }
    });
};

const withNoOffLine = (nextConfig = {}) =>
    Object.assign({}, nextConfig, {
        webpack(config) {
            return config;
        }
    });
const offline = isProd ? withOffline : withNoOffLine;

let configSettings = {
    assetPrefix: 'http://localhost:3000',
    cssLoaderOptions: {
        importLoaders: 1,
        includePaths: /flexboxgrid2/,
        localIdentName: '[local]___[hash:base64:5]'
    },
    cssModules: true,
    enableSvg: false, // for next-fonts
    generateSw: false,
    poweredByHeader: false,
    watchOptions: {
        aggregateTimeout: 300, // delay before rebuilding
        poll: 1000 // Check for changes every second
    },
    workboxOpts: {
        swDest: 'service-worker.js',
        swSrc: 'workbox.config.js'
    }
};

if (isProd) {
    configSettings = Object.assign(
        configSettings,
        {
            stats: {
                warnings: false
            }
        },
        {}
    );
}

module.exports = compose(
    withFonts,
    withCSS,
    withTypescript,
    // withMDX,
    offline,
    withPlugins,
    withEnv(NODE_ENV)
)(configSettings);
