# Interview Challenge

> A project submitted as an interview challenge for a Calgary based company

## Download/Clone

[Download source code](https://bitbucket.org/ali_hmer/sa_movie_locations/)

## Usage

Install node.js (8 or more): [https://nodejs.org](https://nodejs.org)

Install yarn: `npm i -g yarn`

Run commands (source directory):

`yarn` or `npm install`

for development:

`yarn dev` or `npm run dev`

for production:

`yarn build && yarn start` or `npm run build && npm run start`

go to: [http://localhost:3000](http://localhost:3000)
Notice: Google maps key will only work on port 3000 locally. Do not change that port.

for testing in development:
`yarn test:dev`

for testing and code coverage:
`yarn test`

## Libraries

-   **Typescript** 3.0.x
-   **React** 16.8.x
-   **Next.js** 8.0.x
-   **GraphQL** 0.13.x -- Not fully utilized due to time constraints
-   **React Apollo** 2.1.x -- Not fully utilized due to time constraints
-   **Material-UI** 1.4.x
-   **Redux** 4.x
-   **React-intl** 2.4.x
-   _... go to package.json_

## Tools
-   **VS Code**
-   **Nodemon**
-   **Prettier**
-   **TSLint**
-   **Coveralls**

## Code Structure

Most of the code related to `SF Movie Locations` is located in ./client/modules/homePage/ folder(s). The rest is just related to how nextJs  original file structure.

The application will try to fetch only the first 15 movie locations. This can be updated by editing `movie.actions.ts` line 41 and replace it with a reasonable number (something between 50 and 200 would be ok). Remember the `opencage` geodata service licence allows for 2500 requests every day.

Although GraphQL and I18n are added but they are not utilized to the full extent due to time constraints.
Started testing but could not finish it. Again for time constraints and could not do it on the expense of less functionality.


## Improvements

All commands have been tested on Windows, Mac OS and Ubuntu Machines. If you have problems please send me feedback. Thanks.
