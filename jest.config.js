module.exports = {
    collectCoverageFrom: ['src/**/*.{ts,tsx}', '!src/**/*.{spec,builder,type,const,interface,enum}.{ts,tsx}', '!src/index.tsx'],
    coverageDirectory: './reports',
    coveragePathIgnorePatterns: ['<rootDir>/src/types', '<rootDir>/src/typings'],
    coverageReporters: ['lcov'],
    globals: {
        'ts-jest': {
            tsConfig: 'tsconfig.jest.json'
        }
    },
    moduleFileExtensions: ['js', 'json', 'jsx', 'ts', 'tsx'],
    moduleNameMapper: {
        '\\.(css|less)$': 'identity-obj-proxy'
    },
    preset: 'ts-jest',
    reporters: ['default', 'jest-junit'],
    restoreMocks: true,
    roots: ['<rootDir>/src'],
    setupFiles: ['./jest.setup.js'],
    testMatch: ['**/?(*.)spec.ts?(x)'],
    testURL: 'http://localhost/',
    transformIgnorePatterns: ['node_modules']
};
