export type DirectionType = 'ltr' | 'rtl';

export type LocaleType = 'en' | 'fr' | 'ar';

export interface Language {
    locale: LocaleType;
    direction: DirectionType;
}
