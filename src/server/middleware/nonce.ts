import * as express from 'express';
import * as uuid from 'uuid';

export default (app: express.Express) => {
    app.locals.nonce = uuid.v4();
    app.response.locals = { nonce: app.locals.nonce };
};
