import * as express from 'express';
import * as helmet from 'helmet';

const env = process.env.NODE_ENV || 'development';

export default (app: express.Express) => {
    // Content Security Policy
    if (env === 'production') {
        const cspConfig = {
            directives: {
                defaultSrc: ["'self'"],
                scriptSrc: [
                    "'self'",
                    'cdn.polyfill.io',
                    'ajax.googleapis.com',
                    'maxcdn.bootstrapcdn.com',
                    'maxcdn.bootstrapcdn.com',
                    'cdnjs.cloudflare.com',
                    'https://ajax.cloudflare.com',
                    'd2wy8f7a9ursnm.cloudfront.net',
                    'https://notify.bugsnag.com',
                    "'unsafe-inline'",
                    'browser-update.org',
                    // (req, res) => `'nonce-${res.locals.nonce}'`,
                    'maps.googleapis.com',
                    'google-analytics.com',
                    'https://www.google-analytics.com',
                    'https://mts.googleapis.com',
                    'https://storage.googleapis.com',
                    'unpkg.com'
                ],
                styleSrc: [
                    "'self'",
                    // Webpack generates JS that loads our CSS, so this is needed:
                    "'unsafe-inline'",
                    // (req, res) => `'nonce-${res.locals.nonce}'`,
                    'cdnjs.cloudflare.com',
                    'maxcdn.bootstrapcdn.com',
                    'ajax.cloudflare.com',
                    'fonts.googleapis.com',
                    'unpkg.com',
                    'https://use.fontawesome.com',
                    'blob:'
                ],
                imgSrc: [
                    "'self'",
                    'data:',
                    'blob:',
                    'picsum.photos',
                    'csi.gstatic.com',
                    'maps.googleapis.com',
                    'https://notify.bugsnag.com',
                    'https://ajax.cloudflare.com',
                    'maps.gstatic.com',
                    'https://www.google-analytics.com',
                    'https://khms0.googleapis.com',
                    'https://khms1.googleapis.com',
                    'https://images.pexels.com',
                    'https://mts.googleapis.com',
                    'https://material-ui.com'
                ],
                // connectSrc: [
                //   "'self'",
                //   'ws:',
                //   'https://www.google-analytics.com',
                //   'cdnjs.cloudflare.com',
                //   'cdn.polyfill.io',
                //   'csi.gstatic.com',
                //   'google-analytics.com',
                //   'fonts.gstatic.com',
                //   'maps.gstatic.com',
                //   'maps.googleapis.com',
                //   'fonts.googleapis.com',
                //   'https://khms0.googleapis.com',
                //   'https://khms1.googleapis.com'
                // ],

                // Note: Setting this to stricter than * breaks the service worker. :(
                // I can't figure out how to get around this, so if you know of a safer
                // implementation that is kinder to service workers please let me know.
                connectSrc: ['*'], // ["'self'", 'ws:'],
                fontSrc: [
                    "'self'",
                    'cdnjs.cloudflare.com',
                    'https://ajax.cloudflare.com',
                    'https://use.fontawesome.com',
                    'blob:',
                    'data:',
                    'fonts.gstatic.com',
                    'fonts.googleapis.com',
                    'maxcdn.bootstrapcdn.com'
                ],
                objectSrc: ["'none'"],
                mediaSrc: ["'none'"],
                frameSrc: ["'none'"],
                manifestSrc: ["'self'"],
                sandbox: ['allow-forms', 'allow-scripts', 'allow-same-origin', 'allow-popups']
            },
            // This module will detect common mistakes in your directives and throw errors
            // if it finds any. To disable this, enable "loose mode".
            loose: false,

            // Set to true if you only want browsers to report errors, not block them.
            reportOnly: (_, __) => false,
            // You may also set this to a function(req, res) in order to decide dynamically
            // whether to use reportOnly mode, e.g., to allow for a dynamic kill switch.
            // console.log(req) // eslint-disable-line
            //   return true
            // },

            // Set to true if you want to blindly set all headers: Content-Security-Policy,
            // X-WebKit-CSP, and X-Content-Security-Policy.
            setAllHeaders: false,

            // Set to true if you want to disable CSP on Android where it can be buggy.
            disableAndroid: false,

            // Set to false if you want to completely disable any user-agent sniffing.
            // This may make the headers less compatible but it will be much faster.
            // This defaults to `true`.
            browserSniff: true
        };

        app.use(helmet.contentSecurityPolicy(cspConfig));

        app.use(helmet.xssFilter());
        app.use(helmet.frameguard('deny'));
        app.use(helmet.ieNoOpen());
        app.use(helmet.noSniff());
    }
};
