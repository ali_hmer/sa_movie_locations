import accepts from 'accepts';
import * as cookie from 'cookie';
import * as express from 'express';
import { readFileSync } from 'fs';
import { sync } from 'glob';
import { basename } from 'path';
import { DirectionType, LocaleType } from './lang-direction.interface';

const dev: boolean = process.env.NODE_ENV !== 'production';
const rootPath = dev ? '../../../' : '../../';

// We need to expose React Intl's locale data on the request for the user's
// locale. This function will also cache the scripts by lang in memory.
const localeDataCache = new Map();

// We need to load and expose the translations on the request for the user's
// locale. These will only be used in production, in dev the `defaultMessage` in
// each message description in the source code will be used.
const getMessages = (locale: string) => require(`${rootPath}lang/${locale}.json`);

const getLocaleDataScript = (locale: string) => {
    const lang = locale.split('-')[0];
    if (!localeDataCache.has(lang)) {
        const localeDataFile = require.resolve(`react-intl/locale-data/${lang}`);
        const localeDataScript = readFileSync(localeDataFile, 'utf8');
        localeDataCache.set(lang, localeDataScript);
    }
    return localeDataCache.get(lang);
};

export default (req: any, _, next: express.NextFunction) => {
    // When you change language other way (with browser settings is now), you must rewrite get locale from client on this code row
    const languages = sync('./lang/*.json').map((f) => basename(f, '.json').toLowerCase());
    const cookies = cookie.parse(req.headers.cookie || '');
    const requestLocale = accepts(req).language(languages);
    const cookieLocale = languages.indexOf(cookies.locale) >= 0 ? cookies.locale : 'en';

    const locale = (cookieLocale || requestLocale || 'en') as LocaleType;

    req.locale = locale;
    const direction: DirectionType = locale === 'ar' ? 'rtl' : 'ltr'; // cookies.dir || 'ltr';
    req.direction = direction;

    if (locale) {
        req.localeDataScript = getLocaleDataScript(locale);
        req.messages = getMessages(locale);
    }

    return next();
};
