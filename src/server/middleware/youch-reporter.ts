import * as express from 'express';
import * as Youch from 'youch';
import * as forTerminal from 'youch-terminal';

export default (error, req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (!res.headersSent) {
        const youch = new Youch(error, req);
        youch
            .addLink(({ message }) => {
                const url = `https://stackoverflow.com/search?q=${encodeURIComponent(`[adonis.js] ${message}`)}`;
                return `<a href="${url}" target="_blank" title="Search on stackoverflow"><i class="fab fa-stack-overflow"></i></a>`;
            })
            .toHTML()
            .then((response) => {
                res.writeHead(200, { 'content-type': 'text/html' });
                res.write(response);
                res.end();
            })
            .catch((err) => {
                res.writeHead(500);
                res.write(err.message);
                res.end();
            });
        youch.toJSON().then((output) => {
            // tslint:disable-next-line:no-console
            console.log(forTerminal(output));
        });
    } else {
        next(error);
    }
};
