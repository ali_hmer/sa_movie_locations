import express from 'express';
// import * as session from 'express-session';
import * as expressUseragent from 'express-useragent';
import { UserAgent } from 'express-useragent';
import * as IntlPolyfill from 'intl';
import nextjs from 'next';
import StatsD from 'node-statsd';
import { join } from 'path';
import responseTime from 'response-time';
import { parse } from 'url';
import { createApolloServer } from './graphql';
// import helmet from './middleware/helmet';
import languageMiddleware from './middleware/language';
import nonce from './middleware/nonce';
import youchReporter from './middleware/youch-reporter';

// tslint:disable-next-line:no-var-requires
const cookiesMiddleware = require('universal-cookie-express');

// tslint:disable-next-line
require('dotenv').config();

Intl.NumberFormat = IntlPolyfill.NumberFormat;
Intl.DateTimeFormat = IntlPolyfill.DateTimeFormat;

const dev: boolean = process.env.NODE_ENV !== 'production';
const rootPath = dev ? '../../' : '../';

const app = nextjs({ dir: './src', dev, quiet: dev ? false : true });
const handle = app.getRequestHandler();

const runApp = async () => {
    await app.prepare();
    const server = express();
    // helmet(server);
    nonce(server);

    createApolloServer({ introspection: true, playground: true }).applyMiddleware({ app: server });

    server.get('/healthz', (_, res) => {
        res.sendStatus(200);
    });

    server.get('/_info', (_, res) => {
        const { NODE_ENV, NODE_VERSION, LC_CTYPE, BACKEND_ENDPOINT, BUILD_AUTHOR, BUILD_NUM, BUILD_DATE, K8S_NAMESPACE } = process.env;
        const { name, version, description, author, homepage, dependencies } = require(join(__dirname, rootPath, 'package.json'));
        res.json({
            NAME: name,
            DESCRIPTION: description,
            AUTHOR: author,
            VERSION: version,
            HOMEPAGE: homepage,
            BACKEND_ENDPOINT,
            NODE_ENV,
            NODE_VERSION,
            LC_CTYPE,
            BUILD_AUTHOR,
            BUILD_NUM,
            BUILD_DATE,
            K8S_NAMESPACE,
            DEPENDENCIES: dependencies
        });
    });

    server.use(
        '/static',
        express.static(join(__dirname, rootPath, 'static'), {
            maxAge: dev ? '0' : '365d'
        })
    );

    server.disable('x-powered-by');
    server.use(cookiesMiddleware());
    const stats = new StatsD();

    stats.socket.on('error', (error) => {
        // tslint:disable-next-line:no-console
        console.error(error.stack);
    });

    server.use(responseTime());
    server.use(
        responseTime((req, _, time) => {
            const stat = (req.method + req.url)
                .toLowerCase()
                .replace(/[:.]/g, '')
                .replace(/\//g, '_');
            stats.timing(stat, time);
        })
    );

    server.use(expressUseragent.express());

    server.get('/useragent', (req: express.Request, res) => {
        const ua = req.useragent as UserAgent;
        res.send(ua);
    });

    server.get('/useragent', (req: express.Request, res) => {
        const ua = req.useragent as UserAgent;
        res.send(ua);
    });
    server.get('*', languageMiddleware);

    server.get('*', (req: express.Request, res: express.Response) => {
        const parsedUrl = parse(req.url, true);
        const { pathname } = parsedUrl;
        if (!dev && pathname === '/service-worker.js') {
            const filePath = join(__dirname, dev ? '../' : '../src/', '.next', pathname);
            app.serveStatic(req, res, filePath);
        } else {
            return handle(req, res, parsedUrl);
        }
    });

    server.use(youchReporter);

    const PORT = process.env.PORT || 3000;
    server.listen(PORT, (err: Error) => {
        if (err) {
            throw err;
        }
        // tslint:disable-next-line
        console.log(`Server is ready on PORT=${PORT}`);
    });
};

runApp();
