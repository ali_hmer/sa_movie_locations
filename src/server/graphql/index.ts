import { UpdateMeMutationArgs } from '@graphql-model';
import { ApolloServer, Config, gql } from 'apollo-server-express';
import { importSchema } from 'graphql-import';
import { AccountService } from '../services';

type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

const resolvers = {
    Query: {
        me: (_: any) => AccountService.findLoggedUser()
    },

    Mutation: {
        updateMe: (args: UpdateMeMutationArgs) => AccountService.save(args)
    }
};

const typeDefs = gql`
    ${importSchema('src/schema/root.graphql')}
`;

export const createApolloServer = (config?: Omit<Config, 'resolvers' | 'typeDefs'>): ApolloServer => {
    return new ApolloServer({
        ...config,
        typeDefs,
        resolvers
    });
};
