import { NextDocumentContext } from 'next/document';
import { ReactCookieProps } from 'react-cookie';
import { LocaleType } from './locale-type.interface';

export type ContextWithLocale = LocaleType & NextDocumentContext & ReactCookieProps;
