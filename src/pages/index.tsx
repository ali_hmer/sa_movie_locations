import { Grid } from '@material-ui/core';
import * as React from 'react';
import { Layout } from '../client/modules/common/components/page';
import { MapContainer as Map } from '../client/modules/homePage';

export default () => (
    <Layout>
        <Grid container spacing={8} style={{ height: '100%' }}>
            <Grid item xs={12} sm={12}>
                <Map />
            </Grid>
        </Grid>
    </Layout>
);
