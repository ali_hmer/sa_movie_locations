export interface LocaleType {
    locale: string;
    localeDataScript: string;
    direction: string;
    nonce: string;
}
