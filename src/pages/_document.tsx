import autoprefixer from 'autoprefixer';
import Document, { Head, Main, NextScript } from 'next/document';
import postcss from 'postcss';
import * as React from 'react';
import { CookiesProvider } from 'react-cookie';
import { StylesContext } from '../client/styles/StylesContext';
import { ContextWithLocale } from './context-with-locale.type';

const prefixer = postcss([autoprefixer as any]);

// tslint:disable-next-line
const JssProvider = require('react-jss/lib/JssProvider').default;

const generateCss = async (css: string): Promise<string> => {
    if (process.env.NODE_ENV === 'production') {
        return (await prefixer.process(css, { from: undefined })).css;
    }
    return css;
};

export default class extends Document<ContextWithLocale> {
    static async getInitialProps(ctx) {
        const {
            req,
            renderPage,
            res: {
                locals: { nonce }
            }
        } = ctx;

        if (typeof nonce !== 'string') {
            throw new Error('A "nonce" value has not been attached to the response');
        }

        const { locale, localeDataScript, direction } = req;
        const paletteType = getPaletteType(req);

        const pageContext = StylesContext.getPageContext(paletteType, direction === 'rtl');

        const page = renderPage((Component: any) => (props) => {
            return (
                <JssProvider registry={pageContext.sheetsRegistry} generateClassName={pageContext.generateClassName}>
                    <CookiesProvider {...req.universalCookies}>
                        <Component pageContext={pageContext} {...props} cookies={{ paletteType: pageContext.paletteType }} />
                    </CookiesProvider>
                </JssProvider>
            );
        });

        const css = await generateCss(pageContext.sheetsRegistry.toString());
        return {
            ...page,
            pageContext,
            locale,
            localeDataScript,
            paletteType,
            direction,
            nonce,
            styles: <style id="jss-server-side" dangerouslySetInnerHTML={{ __html: css }} />
        };
    }

    render() {
        const { locale, localeDataScript, direction, nonce } = this.props;
        // Polyfill Intl API for older browsers
        const polyfill = `https://cdn.polyfill.io/v2/polyfill.min.js?features=Intl.~locale.${locale}`;
        return (
            <html lang={locale} dir={direction}>
                <Head>
                    {/* {locale === 'ar' && <link href="https://fonts.googleapis.com/css?family=Markazi+Text" rel="stylesheet" />} */}
                    <meta charSet="utf-8" />
                    <meta name="viewport" content="user-scalable=0, initial-scale=1, minimum-scale=1, width=device-width, height=device-height" />
                    <meta name="keywords" content="" />
                    <meta name="description" content="" />
                    <meta name="author" content="Ali Hmer" />
                    <link rel="shortcut icon" type="image/x-icon" href="/static/images/favicon.ico" />
                    <link rel="icon" type="image/x-icon" href="/static/images/favicon.ico" />
                    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" />
                    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
                    <link
                        rel="stylesheet"
                        href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
                        integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
                        crossOrigin="anonymous"
                    />
                </Head>
                <body>
                    <Main />
                    <script nonce={nonce} src={polyfill} />
                    <script nonce={nonce} dangerouslySetInnerHTML={{ __html: localeDataScript }} />
                    <NextScript nonce={nonce} />
                </body>
            </html>
        );
    }
}
function getPaletteType(req: any) {
    const cookies = (req.universalCookies && req.universalCookies.cookies) || {};
    const paletteType = cookies.paletteType;
    return paletteType;
}
