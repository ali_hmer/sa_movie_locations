// import { Head } from 'next/document';
import { ConnectedRouter } from 'connected-next-router';
import withRedux from 'next-redux-wrapper';
import { Container, default as App } from 'next/app';
import { withRouter } from 'next/router';
import * as NProgress from 'nprogress';
import * as React from 'react';
import { ApolloProvider, compose } from 'react-apollo';
import { CookiesProvider, ReactCookieProps, withCookies } from 'react-cookie';
import { Provider } from 'react-redux';
import { makeStore } from '../client/store/makeStore';
import { withApollo, withIntl, withMaterialUi } from '../client/with';

import './nprogress.css';

const isProd: boolean = process.env.NODE_ENV === 'production';
interface Props extends ReactCookieProps {
    Component: any;
    store: any;
    pageProps: any;
    apolloClient: any;
}

class SAMovieLocationsApp extends App<Props> {
    static async getInitialProps({ Component, ctx }: any) {
        return {
            pageProps: {
                ...(Component.getInitialProps ? await Component.getInitialProps(ctx) : {})
            }
        };
    }

    componentDidMount(): void {
        const { router } = this.props;
        if (router) {
            router.events.on('routeChangeStart', () => NProgress.start());
            router.events.on('routeChangeComplete', () => NProgress.done());
            router.events.on('routeChangeError', () => NProgress.done());
        }

        if (isProd && 'serviceWorker' in navigator) {
            navigator.serviceWorker
                .register('/service-worker.js')
                .then((registration) => {
                    // tslint:disable-next-line:no-console
                    console.log('service worker registration successful ', registration);
                })
                .catch((err) => {
                    // tslint:disable-next-line:no-console
                    console.warn('service worker registration failed', err.message);
                });
        }
    }

    componentWillUnmount(): void {
        const { router } = this.props;
        if (router) {
            router.events.off('routeChangeStart', () => undefined);
            router.events.off('routeChangeComplete', () => undefined);
            router.events.off('routeChangeError', () => undefined);
        }
    }

    render() {
        const { Component, store, pageProps, apolloClient } = this.props;
        return (
            store && (
                <CookiesProvider>
                    <ApolloProvider client={apolloClient}>
                        <Container>
                            <Provider store={store}>
                                <ConnectedRouter>
                                    <Component {...pageProps} />
                                </ConnectedRouter>
                            </Provider>
                        </Container>
                    </ApolloProvider>
                </CookiesProvider>
            )
        );
    }
}

export default withRouter(
    withRedux(makeStore, { debug: false })(
        compose(
            withApollo,
            withIntl,
            withMaterialUi,
            withCookies
        )(SAMovieLocationsApp)
    )
);
