import { Lang } from '../../client/Lang';
import { TechnologyProps } from './about-technology.interface';

export const Technologies: TechnologyProps[] = [
    { imgSrc: '/static/images/react.png', title: 'React', description: Lang.TECH_REACT },
    { imgSrc: '/static/images/material-design.png', title: 'Material Design', description: Lang.TECH_MD },
    { imgSrc: '/static/images/redux.png', title: 'Redux', description: Lang.TECH_REDUX },
    { imgSrc: '/static/images/ts.png', title: 'TypeScript', description: Lang.TECH_TS },
    { imgSrc: '/static/images/google-maps.png', title: 'Google Maps', description: Lang.TECH_GOOGLE_MAPS },
    { imgSrc: '/static/images/nextjs.jpeg', title: 'Next.js', description: Lang.TECH_NEXTJS }
];
