import { Grid } from '@material-ui/core';
import * as React from 'react';
import { AboutHeader, AboutPaper, InfoPaper, TechCard } from '../../client/modules/about';
import { Layout } from '../../client/modules/common/components/page';
import { Technologies } from './about-technologies';

export default () => (
    <Layout>
        <AboutHeader />
        <Grid container spacing={8}>
            <Grid item xs={12} sm={6}>
                <InfoPaper />
            </Grid>
            <Grid item xs={12} sm={6}>
                <AboutPaper />
            </Grid>
            {Technologies.map((row) => (
                <Grid item xs={12} sm={6} md={3} key={row.title}>
                    <TechCard {...row} />
                </Grid>
            ))}
        </Grid>
    </Layout>
);
