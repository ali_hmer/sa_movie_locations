export interface TechnologyProps {
    readonly imgSrc: string;
    readonly title: string;
    readonly description: string;
}
