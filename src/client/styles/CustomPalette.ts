import { PaletteOptions } from '@material-ui/core/styles/createPalette';

const light = {
    type: 'light',
    primary: {
        main: '#2196f3',
        light: '#4ca8b5',
        dark: '#004d58',
        contrastText: '#fff'
    },
    secondary: {
        main: '#00a651',
        light: '#54d97f',
        dark: '#007625',
        contrastText: '#fff'
    },
    custom: {
        green: {
            main: '#4caf50',
            light: '#80e27e',
            dark: '#087f23',
            contrastText: '#fff'
        },
        orange: {
            main: '#ff9800',
            light: '#ffc947',
            dark: '#c66900',
            contrastText: '#000'
        },
        indigo: {
            main: '#3f51b5',
            light: '#757de8',
            dark: '#002984',
            contrastText: '#fff'
        },
        brown: {
            main: '#795548',
            light: '#a98274',
            dark: '#4b2c20',
            contrastText: '#fff'
        },
        MuiCard: {
            boxShadow: '0 13px 10px -7px rgba(0,0,0,.1)'
        }
    },
    background: {
        default: '#d2dbdd'
    }
} as PaletteOptions;

const dark = {
    type: 'dark',
    primary: {
        main: '#2196f3',
        light: '#4ca8b5',
        dark: '#004d58',
        contrastText: '#fff'
    },
    secondary: {
        main: '#00a651',
        light: '#54d97f',
        dark: '#007625',
        contrastText: '#fff'
    },
    custom: {
        green: {
            main: '#4caf50',
            light: '#80e27e',
            dark: '#087f23',
            contrastText: '#fff'
        },
        orange: {
            main: '#ff9800',
            light: '#ffc947',
            dark: '#c66900',
            contrastText: '#000'
        },
        indigo: {
            main: '#3f51b5',
            light: '#757de8',
            dark: '#002984',
            contrastText: '#fff'
        },
        brown: {
            main: '#795548',
            light: '#a98274',
            dark: '#4b2c20',
            contrastText: '#fff'
        }
    }
} as PaletteOptions;

export const CustomPalette = {
    light,
    dark
};
