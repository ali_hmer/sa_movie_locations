import { createGenerateClassName, createMuiTheme, jssPreset, PaletteType } from '@material-ui/core';

import { CustomPalette } from './CustomPalette';

// tslint:disable-next-line
const { create, SheetsRegistry } = require('jss');

declare const process: any;
declare const global: any;

const createPageContext = (paletteType: PaletteType, isRtl: boolean) => {
    let rtl = () => null;
    if (isRtl) {
        rtl = require('jss-rtl').default;
        require('./ar-font.css');
    }
    // Configure JSS
    const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

    return {
        paletteType,
        isRtl,
        jss,
        theme: createTheme(isRtl, paletteType),
        // theme: getTheme(palette),
        // This is needed in order to deduplicate the injection of CSS in the page.
        sheetsManager: new Map(),
        // This is needed in order to inject the critical CSS.
        sheetsRegistry: new SheetsRegistry(),
        generateClassName: createGenerateClassName({ productionPrefix: 'j' })
    };
};

export interface PageContext {
    readonly jss: any;
    readonly sheetsRegistry: any;
    readonly generateClassName: any;
    readonly sheetsManager: any;
    readonly theme: any;
}

export const StylesContext = {
    getPageContext(paletteType: PaletteType, isRtl: boolean) {
        // Make sure to create a new store for every server-side request so that data
        // isn't shared between connections (which would be bad)
        if (!process.browser) {
            return createPageContext(paletteType, isRtl);
        }

        // Reuse context on the client-side
        this.setGlobalMaterialUI(paletteType, isRtl);

        return global.__INIT_MATERIAL_UI__;
    },

    setGlobalMaterialUI(paletteType: PaletteType, isRtl: boolean) {
        if (!global.__INIT_MATERIAL_UI__ || global.__INIT_MATERIAL_UI__.theme.palette.type !== paletteType) {
            global.__INIT_MATERIAL_UI__ = createPageContext(paletteType, isRtl);
        }
    },

    updatePageContext(paletteType: PaletteType, isRtl: boolean) {
        const pageContext = {
            ...global.__MUI_PAGE_CONTEXT__,
            direction: isRtl ? 'rtl' : 'ltr',
            theme: createTheme(isRtl, paletteType)
        };
        global.__MUI_PAGE_CONTEXT__ = pageContext;

        return pageContext;
    }
};
function createTheme(isRtl: boolean, paletteType: string) {
    return createMuiTheme({
        direction: isRtl ? 'rtl' : 'ltr',
        palette: paletteType && CustomPalette[paletteType],
        typography: {
            // fontFamily: isRtl ? '"Markazi Text", "Roboto", serif' : '"Roboto", "Helvetica", "Arial", sans-serif',
            fontFamily: isRtl ? '"Montserrat Arabic", "Roboto", serif' : '"Roboto", "Helvetica", "Arial", sans-serif',
            fontSize: isRtl ? 14 : 14,
            fontWeightLight: 300,
            fontWeightRegular: 400,
            fontWeightMedium: 500,
            useNextVariants: true
        },
        overrides: {
            MuiCard: {
                root: {
                    boxShadow: '0 13px 10px -7px rgba(0,0,0,.1)',
                    borderRadius: '0'
                }
            },
            MuiPaper: {
                root: {
                    boxShadow: '0 13px 10px -7px rgba(0,0,0,.1)',
                    borderRadius: '0'
                },
                elevation1: {
                    boxShadow: '0 13px 10px -7px rgba(0,0,0,.1)'
                },
                elevation2: {
                    boxShadow: '0 13px 10px -7px rgba(0,0,0,.1)'
                },
                rounded: {
                    borderRadius: '0'
                }
            },
            MuiButton: {
                // Name of the component ⚛️ / style sheet
                text: {
                    // Name of the rule
                    color: 'white' // Some CSS
                }
            }
        }
    });
}
