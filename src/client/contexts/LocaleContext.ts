import * as React from 'react';

export type LocaleType = 'ar' | 'en';

const defaultLocale = 'en';
const getLocaleType = (): LocaleType => {
    try {
        const locale = localStorage.getItem('locale') as LocaleType;
        if (locale) {
            return locale;
        }
        return defaultLocale;
    } catch (err) {
        return defaultLocale;
    }
};

const saveLocaleType = (type: LocaleType) => {
    return localStorage && localStorage.setItem('locale', JSON.stringify(type));
};

export interface LocaleContextType {
    readonly localeType: LocaleType;
    readonly toggleLocale: () => void;
}

const localeTypeFromStorage: LocaleType = getLocaleType();

const LocaleContext = React.createContext<LocaleContextType>({
    localeType: defaultLocale,
    toggleLocale: () => {
        // declaration in constructor with state change
    }
});

export { getLocaleType, saveLocaleType, localeTypeFromStorage, LocaleContext };
