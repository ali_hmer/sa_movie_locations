import { PaletteType } from '@material-ui/core';
import * as React from 'react';

declare const process: any;

const defaultTheme: PaletteType = 'light';

const getPaletteType = (): PaletteType => {
    try {
        if (process.browser) {
            const pt = localStorage.getItem('paletteType') as PaletteType;
            return JSON.parse(pt) || defaultTheme;
        } else {
            return defaultTheme;
        }
    } catch (err) {
        return defaultTheme;
    }
};

const savePaletteType = (type: PaletteType) => {
    try {
        localStorage.setItem('paletteType', JSON.stringify(type));
    } catch (err) {
        // nothing...
    }
};

export interface ThemeContextType {
    readonly paletteType: PaletteType;
    readonly toggleTheme: () => void;
}

const paletteTypeFromStorage = (): PaletteType => getPaletteType();

const ThemeContext = React.createContext<ThemeContextType>({
    paletteType: defaultTheme,
    toggleTheme: () => {
        // declaration in constructor with state change
    }
});

export { getPaletteType, savePaletteType, paletteTypeFromStorage, ThemeContext };
