import { createRouterMiddleware, initialRouterState } from 'connected-next-router';
import { applyMiddleware, compose, createStore, StoreCreator } from 'redux';
import reduxPromise from 'redux-promise';
import thunkMiddleware from 'redux-thunk';

import { rootReducer } from './rootReducer';

declare const window: any;

const isReduxDevTools = (isServer: boolean) => !isServer && window.__REDUX_DEVTOOLS_EXTENSION__ && process.env.NODE_ENV === 'development';

const reduxTools = (store: StoreCreator, isServer: boolean) => (isReduxDevTools(isServer) ? window.__REDUX_DEVTOOLS_EXTENSION__()(store) : store);

const routerMiddleware = createRouterMiddleware();
const middleware = [routerMiddleware, thunkMiddleware, reduxPromise];

export const makeStore = (initialState: any = {}, { isServer, asPath }: { isServer: boolean; asPath: string }) => {
    if (isServer) {
        initialState.router = initialRouterState(asPath);
    }

    return compose(applyMiddleware(...middleware))(reduxTools(createStore, isServer))(rootReducer, initialState);
};
