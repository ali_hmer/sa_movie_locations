import { routerReducer } from 'connected-next-router';
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { MovieLocationReducer } from '../modules/homePage/reducers/movie-location.reducer';
import { MovieReducer } from '../modules/homePage/reducers/movie.reducer';
import { IStore } from './store.interface';

type Reducers = { [P in keyof IStore]: any };

export const rootReducer = combineReducers<Reducers>({
    form: formReducer,
    movies: MovieReducer,
    movieLocations: MovieLocationReducer,
    router: routerReducer
});
