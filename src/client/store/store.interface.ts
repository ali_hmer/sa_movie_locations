import { FormStateMap } from 'redux-form';
import { IMovieLocationStore } from '../modules/homePage/reducers/movie-locations.store.interface';
import { IMovieStore } from '../modules/homePage/reducers/movie.store.interface';

export interface IStore {
    readonly form: FormStateMap;
    readonly movies: IMovieStore;
    readonly movieLocations: IMovieLocationStore;
    readonly router: any;
}
