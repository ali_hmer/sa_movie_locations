export interface RouteType {
    name: JSX.Element;

    route: string;

    icon: string;

    divider: boolean;
}
