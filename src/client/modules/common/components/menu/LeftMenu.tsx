import { Drawer, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { default as Icon } from '@material-ui/core/Icon';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import withStyles, { WithStyles } from '@material-ui/core/styles/withStyles';
import { default as Link } from 'next/link';
import * as React from 'react';
import { RouteType } from './route.interface';
import { routes } from './routes';

const styles = (_: Theme) => ({
    list: {
        width: 250
    }
});
interface Props extends WithStyles<typeof styles> {
    readonly open: boolean;
    readonly onClose: () => void;
}

const decorate = withStyles(styles);

export const LeftMenu = decorate<React.ComponentType<Props>>(({ open, onClose, classes }) => (
    <Drawer open={open} onClose={onClose}>
        <div tabIndex={0} role="button" onClick={onClose} onKeyDown={onClose}>
            <div className={classes.list}>
                <List>
                    {routes.map((item: RouteType) => (
                        <Link prefetch href={item.route} key={item.route} replace>
                            <ListItem button divider={item.divider}>
                                <ListItemIcon>
                                    <Icon>{item.icon}</Icon>
                                </ListItemIcon>
                                <ListItemText primary={item.name} />
                            </ListItem>
                        </Link>
                    ))}
                </List>
            </div>
        </div>
    </Drawer>
));
