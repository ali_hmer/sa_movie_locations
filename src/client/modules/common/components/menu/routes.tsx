import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Lang } from '../../../../Lang';
import { RouteType } from './route.interface';

const formatMessage = (name) => {
    return <FormattedMessage id={Lang[name]}>{(msg) => <span>{msg}</span>}</FormattedMessage>;
};

export const routes: RouteType[] = [
    { name: formatMessage('MAINMENU_HOME'), route: '/', icon: 'home', divider: false },
    { name: formatMessage('MAINMENU_ABOUT'), route: '/about', icon: 'info', divider: true }
];
