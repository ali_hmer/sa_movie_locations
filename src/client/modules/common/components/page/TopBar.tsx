import { fade } from '@material-ui/core/styles/colorManipulator';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import withStyles, { WithStyles } from '@material-ui/core/styles/withStyles';
import { Menu as MenuIcon } from '@material-ui/icons';
import { default as Link } from 'next/link';
import * as React from 'react';
import HeadRoom from 'react-headroom';
import { FormattedMessage } from 'react-intl';

import { LocaleContext, ThemeContext } from '../../../../contexts';
import { Lang } from '../../../../Lang';
import { LightBulbFullIcon, LightBulbOutlineIcon } from '../icon';

import { AppBar, IconButton, Toolbar, Typography } from '@material-ui/core';
import './TopBar.css';

// declare const process: any;

// tslint:disable-next-line:no-var-requires
// const css = require('./TopBar.css');

const styles = (theme: Theme) => ({
    flex: {
        flex: 1
    },
    grow: {
        flexGrow: 1
    },
    title: {
        cursor: 'pointer'
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20
    },
    externalLink: {
        textDecoration: 'none'
    },
    iconColor: {
        color: theme.palette.primary.contrastText
    },
    search: {
        position: 'relative' as 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25)
        },
        marginLeft: '16px',
        marginRight: '16px',
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing.unit,
            width: 'auto'
        }
    },
    searchIcon: {
        width: theme.spacing.unit * 9,
        height: '100%',
        position: 'absolute' as 'absolute',
        pointerEvents: 'none' as 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputRoot: {
        color: 'inherit',
        width: '100%'
    },
    inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 10,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: 120,
            '&:focus': {
                width: 200
            }
        }
    }
});

interface Props extends WithStyles<typeof styles> {
    readonly bitBucketUrl: string;
    readonly onClickOpenLeftMenu: () => void;
}

const decorate = withStyles(styles);

export const TopBar = decorate<React.ComponentType<Props>>(({ bitBucketUrl, classes, onClickOpenLeftMenu }) => (
    <HeadRoom className="header" pinStart={10} wrapperStyle={{ marginBottom: '10px' }} disableInlineStyles>
        <AppBar position="sticky">
            <Toolbar>
                <IconButton className={classes.menuButton} color="inherit" aria-label="Menu" onClick={onClickOpenLeftMenu}>
                    <MenuIcon />
                </IconButton>
                <FormattedMessage id={Lang.TITLE}>
                    {(msg) => (
                        <Link href={'/'}>
                            <Typography variant="h6" color="inherit" className={classes.title}>
                                {msg}
                            </Typography>
                        </Link>
                    )}
                </FormattedMessage>
                <div className={classes.flex} />
                <div className={classes.grow} />
                <LocaleContext.Consumer>
                    {({ /* localeType, */ toggleLocale }) => (
                        <IconButton color="inherit" onClick={toggleLocale}>
                            {/* <Typography color="inherit">{localeType.toUpperCase()}</Typography> */}
                        </IconButton>
                    )}
                </LocaleContext.Consumer>
                <ThemeContext.Consumer>
                    {({ paletteType, toggleTheme }) => (
                        <IconButton color="inherit" onClick={toggleTheme}>
                            {paletteType === 'light' ? <LightBulbFullIcon /> : <LightBulbOutlineIcon />}
                        </IconButton>
                    )}
                </ThemeContext.Consumer>
                <a href={bitBucketUrl} target="_blank" className={classes.externalLink}>
                    <IconButton>
                        <i className={`fab fa-bitbucket ${classes.iconColor}`} />
                    </IconButton>
                </a>
            </Toolbar>
        </AppBar>
    </HeadRoom>
));
