import * as React from 'react';
import { Content, LeftMenu, TopBar } from '..';

interface Props {}

const initialState = {
    isOpenLeftMenu: false
};

export class Layout extends React.Component<Props, typeof initialState> {
    readonly state = initialState;

    handleOnOpenLeftMenu = () => {
        this.setState((prevState) => ({ ...prevState, isOpenLeftMenu: true }));
    };

    handleOnCloseLeftMenu = () => {
        this.setState((prevState) => ({ ...prevState, isOpenLeftMenu: false }));
    };

    render() {
        const { children } = this.props;
        const { isOpenLeftMenu } = this.state;

        return (
            <>
                <TopBar bitBucketUrl={'https://bitbucket.org/ali_hmer/sa_movie_locations'} onClickOpenLeftMenu={this.handleOnOpenLeftMenu} />
                <LeftMenu open={isOpenLeftMenu} onClose={this.handleOnCloseLeftMenu} />
                <Content>{children}</Content>
            </>
        );
    }
}
