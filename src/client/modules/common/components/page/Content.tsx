import { Theme } from '@material-ui/core/styles/createMuiTheme';
import withStyles, { WithStyles } from '@material-ui/core/styles/withStyles';
import * as React from 'react';

const styles = (theme: Theme) => ({
    root: {
        padding: theme.spacing.unit,
        height: '90vh'
    }
});
interface Props extends WithStyles<typeof styles> {}

const decorate = withStyles(styles);

export const Content = decorate<React.ComponentType<Props>>(({ children, classes }) => <div className={classes.root}>{children}</div>);
