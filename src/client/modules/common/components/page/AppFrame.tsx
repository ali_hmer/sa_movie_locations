import { Theme } from '@material-ui/core/styles/createMuiTheme';
import withStyles, { WithStyles } from '@material-ui/core/styles/withStyles';
import * as React from 'react';

const styles = (theme: Theme) => ({
    '@global': {
        html: {
            background: theme.palette.background.default,
            WebkitFontSmoothing: 'antialiased', // Antialiasing.
            MozOsxFontSmoothing: 'grayscale', // Antialiasing.
            height: '100vh'
        },
        body: {
            margin: 0,
            height: '100vh'
        }
    } as any
});

interface Props extends WithStyles<typeof styles> {}

const decorate = withStyles(styles);

export const AppFrame = decorate<React.ComponentType<Props>>(({ children }) => <div id="loadingContent">{children}</div>);
