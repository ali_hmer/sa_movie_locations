import { WithStyles } from '@material-ui/core/styles/withStyles';
import { AboutPaperStyles } from './about-paper.styles';

export interface AboutPaperProps extends WithStyles<typeof AboutPaperStyles> {}
