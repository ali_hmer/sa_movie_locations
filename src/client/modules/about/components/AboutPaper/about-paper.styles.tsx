import { Theme } from '@material-ui/core/styles/createMuiTheme';

export const AboutPaperStyles = (theme: Theme) => ({
    root: {
        padding: theme.spacing.unit
    },
    checkItemIcon: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,
        borderRadius: 2
    }
});
