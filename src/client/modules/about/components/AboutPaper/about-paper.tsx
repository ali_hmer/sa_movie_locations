import withStyles from '@material-ui/core/styles/withStyles';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';

import { FormHelperText, List, Paper, Typography } from '@material-ui/core';
import { Lang } from '../../../../Lang';
import { AboutPaperProps } from './about-paper.props.interface';
import { AboutPaperStyles } from './about-paper.styles';

const decorate = withStyles(AboutPaperStyles);

export const AboutPaper = decorate<React.ComponentType<AboutPaperProps>>(({ classes }) => {
    return (
        <Paper className={classes.root}>
            <FormattedMessage id={Lang.ABOUT_DESCRIPTION}>{(msg) => <Typography variant="subtitle1">{msg}</Typography>}</FormattedMessage>
            <List dense>
                <FormattedMessage id={Lang.ABOUT_WITHTYPESCRIPT}>{(msg) => <FormHelperText>{msg}</FormHelperText>}</FormattedMessage>
            </List>
        </Paper>
    );
});
