import { Paper, Typography } from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Lang } from '../../../../Lang';
import { InfoPaperProps } from './info-paper.props.interface';
import { InfoPaperStyles } from './info-paper.styles';

const decorate = withStyles(InfoPaperStyles);

export const InfoPaper = decorate<React.ComponentType<InfoPaperProps>>(({ classes }) => {
    return (
        <Paper className={classes.root}>
            <FormattedMessage id={Lang.INFO_TITLE}>{(msg) => <Typography variant="subtitle1">{msg}</Typography>}</FormattedMessage>
            <FormattedMessage id={Lang.INFO_DESCRIPTION}>
                {(msg) => (
                    <Typography variant="caption" className={classes.description}>
                        {msg}
                    </Typography>
                )}
            </FormattedMessage>

            <div className={classes.iconLinkWrapper}>
                <div>
                    <a href={'https://bitbucket.org/ali_hmer/sa_movie_locations'} target="_blank" className={classes.iconLink}>
                        <i className={`fab fa-bitbucket fa-2x`} />
                    </a>
                </div>
            </div>
        </Paper>
    );
});
