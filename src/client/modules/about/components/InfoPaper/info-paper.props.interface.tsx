import { WithStyles } from '@material-ui/core/styles/withStyles';
import { InfoPaperStyles } from './info-paper.styles';

export interface InfoPaperProps extends WithStyles<typeof InfoPaperStyles> {}
