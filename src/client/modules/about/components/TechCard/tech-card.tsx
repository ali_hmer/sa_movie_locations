import { Paper, Typography } from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { TechCardProps } from './tech-card.props.interface';
import { TechCardStyles } from './tech-card.styles';

const decorate = withStyles(TechCardStyles);

export const TechCard = decorate<React.ComponentType<TechCardProps>>(({ classes, imgSrc, title, description }) => (
    <Paper className={classes.root}>
        <div className={classes.content}>
            <div className={classes.mediaWrapper}>
                <img src={imgSrc} title={title} className={classes.media} />
            </div>
            <Typography gutterBottom variant="body2" component="sub">
                {title}
            </Typography>
        </div>
        <div className={classes.content}>
            <FormattedMessage id={description}>{(msg) => <Typography variant="caption">{msg}</Typography>}</FormattedMessage>
        </div>
    </Paper>
));
