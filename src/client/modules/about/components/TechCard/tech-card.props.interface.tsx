import { WithStyles } from '@material-ui/core/styles/withStyles';
import { TechCardStyles } from './tech-card.styles';

export interface TechCardProps extends WithStyles<typeof TechCardStyles> {
    readonly imgSrc: string;
    readonly title: string;
    readonly description: string;
}
