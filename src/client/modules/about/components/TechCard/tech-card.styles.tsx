import { Theme } from '@material-ui/core/styles/createMuiTheme';

export const TechCardStyles = (theme: Theme) => ({
    root: {
        marginTop: 20
    },
    mediaWrapper: {
        float: 'left' as 'left',
        marginTop: -20,
        marginRight: 15,
        borderRadius: 4,
        padding: 8,
        backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 100 : 500],
        boxShadow: '0px 1px 5px 0px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12)'
    },
    media: {
        height: 50,
        'object-fit': 'contain'
    },
    content: {
        padding: theme.spacing.unit,
        clear: 'left' as 'left'
    }
});
