import { Theme } from '@material-ui/core/styles/createMuiTheme';

export const AboutHeaderStyles = (theme: Theme) => ({
    root: {
        padding: theme.spacing.unit
    },
    description: {
        paddingTop: theme.spacing.unit
    },
    iconLinkWrapper: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: theme.spacing.unit
    },
    iconLink: {
        textDecoration: 'none',
        color: theme.palette.secondary.main,
        paddingRight: theme.spacing.unit
    }
});
