import { Typography } from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Lang } from '../../../../Lang';
import { AboutHeaderProps } from './about-header.props.interface';
import { AboutHeaderStyles } from './about-header.styles';

const decorate = withStyles(AboutHeaderStyles);

export const AboutHeader = decorate<React.ComponentType<AboutHeaderProps>>(({ classes }) => {
    return (
        <FormattedMessage id={Lang.ABOUT_HEADER}>
            {(msg) => (
                <Typography gutterBottom variant="h5" component="h2" align="center" className={classes.description}>
                    {msg}
                </Typography>
            )}
        </FormattedMessage>
    );
});
