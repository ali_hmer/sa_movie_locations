import { WithStyles } from '@material-ui/core/styles/withStyles';
import { AboutHeaderStyles } from './about-header.styles';

export interface AboutHeaderProps extends WithStyles<typeof AboutHeaderStyles> {}
