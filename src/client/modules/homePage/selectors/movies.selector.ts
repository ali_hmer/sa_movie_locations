import { IStore } from '../../../store/store.interface';
import { IMovieLocationStore } from '../reducers/movie-locations.store.interface';
import { IMovieStore } from '../reducers/movie.store.interface';
/**
 * Selector for data that is relevant to the movies and their geometry locations from application state.
 */
export class MoviesSelectors {
    /**
     * Returns movies data.
     *
     * @param state     The current application state.
     */
    public static MoviesData(state: IStore): IMovieStore {
        return state.movies;
    }

    /**
     * Returns movie locations data.
     *
     * @param state     The current application state.
     */
    public static MovieLocationsData(state: IStore): IMovieLocationStore {
        return state.movieLocations;
    }
}
