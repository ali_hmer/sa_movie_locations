// define the basic color of your map, plus a value for saturation and brightness
const $mainColor = '#2d313f';
const $saturation = -20;
const $brightness = 5;

// we define here the style of the map
export const mapStyle = [
    {
        // set saturation for the labels on the map
        elementType: 'labels',
        stylers: [{ saturation: $saturation }]
    },
    {
        // poi stands for point of interest - don't show these lables on the map
        elementType: 'labels',
        featureType: 'poi',
        stylers: [{ visibility: 'off' }]
    },
    {
        // don't show highways lables on the map
        elementType: 'labels',
        featureType: 'road.highway',
        stylers: [{ visibility: 'off' }]
    },
    {
        // don't show local road lables on the map
        elementType: 'labels.icon',
        featureType: 'road.local',
        stylers: [{ visibility: 'off' }]
    },
    {
        // don't show arterial road lables on the map
        elementType: 'labels.icon',
        featureType: 'road.arterial',
        stylers: [{ visibility: 'off' }]
    },
    {
        // don't show road lables on the map
        elementType: 'geometry.stroke',
        featureType: 'road',
        stylers: [{ visibility: 'off' }]
    },

    // style different elements on the map
    {
        elementType: 'geometry.fill',
        featureType: 'transit',
        stylers: [{ hue: $mainColor }, { visibility: 'on' }, { lightness: $brightness }, { saturation: $saturation }]
    },
    {
        elementType: 'geometry.fill',
        featureType: 'poi',
        stylers: [{ hue: $mainColor }, { visibility: 'on' }, { lightness: $brightness }, { saturation: $saturation }]
    },
    {
        elementType: 'geometry.fill',
        featureType: 'poi.government',
        stylers: [{ hue: $mainColor }, { visibility: 'on' }, { lightness: $brightness }, { saturation: $saturation }]
    },
    {
        elementType: 'geometry.fill',
        featureType: 'poi.sports_complex',
        stylers: [{ hue: $mainColor }, { visibility: 'on' }, { lightness: $brightness }, { saturation: $saturation }]
    },
    {
        elementType: 'geometry.fill',
        featureType: 'poi.attraction',
        stylers: [{ hue: $mainColor }, { visibility: 'on' }, { lightness: $brightness }, { saturation: $saturation }]
    },
    {
        elementType: 'geometry.fill',
        featureType: 'poi.business',
        stylers: [{ hue: $mainColor }, { visibility: 'on' }, { lightness: $brightness }, { saturation: $saturation }]
    },
    {
        elementType: 'geometry.fill',
        featureType: 'transit',
        stylers: [{ hue: $mainColor }, { visibility: 'on' }, { lightness: $brightness }, { saturation: $saturation }]
    },
    {
        elementType: 'geometry.fill',
        featureType: 'transit.station',
        stylers: [{ hue: $mainColor }, { visibility: 'on' }, { lightness: $brightness }, { saturation: $saturation }]
    },
    {
        featureType: 'landscape',
        stylers: [{ hue: $mainColor }, { visibility: 'on' }, { lightness: $brightness }, { saturation: $saturation }]
    },
    {
        elementType: 'geometry.fill',
        featureType: 'road',
        stylers: [{ hue: $mainColor }, { visibility: 'on' }, { lightness: $brightness }, { saturation: $saturation }]
    },
    {
        elementType: 'geometry.fill',
        featureType: 'road.highway',
        stylers: [{ hue: $mainColor }, { visibility: 'on' }, { lightness: $brightness }, { saturation: $saturation }]
    },
    {
        elementType: 'geometry',
        featureType: 'water',
        stylers: [{ hue: $mainColor }, { visibility: 'on' }, { lightness: $brightness }, { saturation: $saturation }]
    }
];
