import { Theme } from '@material-ui/core/styles';
import { IMovie } from '../../reducers/movie.interface';

export interface IMovieSearchOwnProps {
    classes: any;
    theme: Theme;
}

export interface ISearchSuggestion {
    label: string;
    value: string;
}
export interface IMovieSearchStateProps {
    searchSuggestions: ISearchSuggestion[];
    movies: {
        [id: string]: IMovie;
    };
}

export interface IMovieSearchProps extends IMovieSearchOwnProps, IMovieSearchStateProps, IMovieSearchDispatchProps {}

export interface IMovieSearchState {
    selected: string;
}

export interface IMovieSearchDispatchProps {
    /**
     * Emitted for fetching the selected movie locations.
     */
    fetchMovieLocations: (movies: { [id: string]: IMovie }) => void;
}
