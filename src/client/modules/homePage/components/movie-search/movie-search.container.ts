import { withStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { createSelector } from 'reselect';
import { IStore } from 'src/client/store/store.interface';
import { MovieDataActions } from '../../actions/movie.actions';
import { IMovie } from '../../reducers/movie.interface';
import { MoviesSelectors } from '../../selectors/movies.selector';
import { MovieSearch } from './movie-search.component';
import { IMovieSearchDispatchProps, IMovieSearchOwnProps, IMovieSearchStateProps } from './movie-search.props.interface';
import { MovieSearchStyles } from './movie-search.styles';

const decorate = withStyles(MovieSearchStyles, { withTheme: true });

export const mapStateToProps = () => {
    const selectMovies = createSelector(
        MoviesSelectors.MoviesData,
        (data) => ({
            movies: data.movies,
            moviesIsFetched: data.isFetched
        })
    );

    return (state: IStore, _: IMovieSearchOwnProps): IMovieSearchStateProps => {
        const movies = selectMovies(state).movies;
        const keys = Object.keys(movies);

        return {
            searchSuggestions: [
                ...new Set(
                    keys.map((id) => ({
                        label: movies[id].title,
                        value: id
                    }))
                )
            ],
            movies
        };
    };
};

export function mapDispatchToProps(dispatch: Dispatch, _: IMovieSearchOwnProps): IMovieSearchDispatchProps {
    return {
        fetchMovieLocations: async (movies: { [id: string]: IMovie }) => await MovieDataActions(dispatch).fetchMovieLocations(movies)
    };
}

export const MovieSearchContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(decorate(MovieSearch));
