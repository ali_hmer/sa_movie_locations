import { shallow } from 'enzyme';
import React from 'react';
import { Marker } from 'react-google-maps';
import { IMovie } from '../../reducers/movie.interface';
import { MapMarker } from './map-marker.component';
import { IMarkerProps } from './map-marker.props.interface';

describe('Components', () => {
    describe('MapMarker', () => {
        let props: IMarkerProps;

        beforeEach(() => {
            props = {
                movie: {} as IMovie,
                id: '',
                lat: 10,
                lng: 10
            };
        });

        it('should render marker component', () => {
            const component = shallow(<MapMarker {...props} />);

            const marker = component.find(Marker);

            expect(marker.length).toBe(1);
        });
    });
});
