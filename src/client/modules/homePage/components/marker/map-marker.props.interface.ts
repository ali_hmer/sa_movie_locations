import { IMovie } from '../../reducers/movie.interface';

export interface IMarkerOwnProps {
    movie: IMovie;
    id: string;
    lat: number;
    lng: number;
}
export interface IMarkerProps extends IMarkerOwnProps {}

export interface IMarkerState {
    isOpen: boolean;
}
