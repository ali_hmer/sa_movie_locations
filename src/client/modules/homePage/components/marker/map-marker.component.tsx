import * as React from 'react';
import { Marker } from 'react-google-maps';
import { IMarkerProps, IMarkerState } from './map-marker.props.interface';

// tslint:disable-next-line:no-var-requires
const { InfoBox } = require('react-google-maps/lib/components/addons/InfoBox');

export class MapMarker extends React.Component<IMarkerProps, IMarkerState> {
    constructor(props) {
        super(props);
        this.state = { isOpen: false };
    }

    private onToggleOpen = () => this.setState((state) => ({ isOpen: !state.isOpen }));

    public render() {
        const { lat, lng, movie, id } = this.props;

        return (
            <Marker key={id} position={{ lat, lng }} onClick={this.onToggleOpen}>
                {this.state.isOpen && (
                    <InfoBox onClick={this.onToggleOpen}>
                        <div style={{ backgroundColor: `#00a1f7`, opacity: 0.85, padding: `12px` }}>
                            <div style={{ fontSize: `16px`, color: `white` }}>
                                {movie.title}, {movie.release_year}, {movie.director}
                            </div>
                        </div>
                    </InfoBox>
                )}
            </Marker>
        );
    }
}
