import { withStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { createSelector } from 'reselect';
import { IStore } from '../../../store/store.interface';
import { MovieDataActions } from '../actions/movie.actions';
import { IMovie } from '../reducers/movie.interface';
import { MoviesSelectors } from '../selectors/movies.selector';
import { Map } from './map.component';
import { IMapDataDispatchProps, IMapDataOwnProps, IMapDataStateProps } from './map.props.interface';
import { MapStyles } from './map.styles';

const decorate = withStyles(MapStyles);

export const mapStateToProps = () => {
    const selectMovies = createSelector(
        MoviesSelectors.MoviesData,
        (data) => ({
            movies: data.movies,
            moviesIsFetched: data.isFetched
        })
    );

    const selectMovieLocations = createSelector(
        MoviesSelectors.MovieLocationsData,
        (data) => ({
            movieLocations: data.movieLocations,
            movieLocationsIsFetched: data.isFetched,
            movieLocationsIsFetching: data.isFetching
        })
    );
    return (state: IStore, _: IMapDataOwnProps): IMapDataStateProps => ({
        moviesIsFetched: selectMovies(state).moviesIsFetched,
        movies: selectMovies(state).movies,
        movieLocations: selectMovieLocations(state).movieLocations,
        movieLocationsIsFetched: selectMovieLocations(state).movieLocationsIsFetched,
        movieLocationsIsFetching: selectMovieLocations(state).movieLocationsIsFetching
    });
};

export function mapDispatchToProps(dispatch: Dispatch, _: IMapDataOwnProps): IMapDataDispatchProps {
    return {
        fetchMoviesData: async () => await MovieDataActions(dispatch).fetchMoviesData(),
        fetchMovieLocations: async (movies: { [id: string]: IMovie }) => await MovieDataActions(dispatch).fetchMovieLocations(movies)
    };
}

export const MapContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(decorate(Map));
