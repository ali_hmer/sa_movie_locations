import { Theme } from '@material-ui/core/styles/createMuiTheme';

export const MapStyles = (theme: Theme) => ({
    root: {
        padding: theme.spacing.unit,
        height: '90vh'
    },
    description: {
        paddingTop: theme.spacing.unit
    },
    iconLinkWrapper: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: theme.spacing.unit
    },
    iconLink: {
        textDecoration: 'none',
        color: theme.palette.secondary.main,
        paddingRight: theme.spacing.unit
    }
});
