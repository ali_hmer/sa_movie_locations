import { WithStyles } from '@material-ui/core/styles/withStyles';
import { IMarker, IMovieModelLocation } from '../reducers/movie-location.interface';
import { IMovie } from '../reducers/movie.interface';
import { MapStyles } from './map.styles';

export interface IMapDataProps extends WithStyles<typeof MapStyles>, IMapDataStateProps, IMapDataOwnProps, IMapDataDispatchProps {}

export interface IMapDataOwnProps {}

export interface IMapDataStateProps {
    movies: { [id: string]: IMovie };
    movieLocations: IMovieModelLocation;
    moviesIsFetched: boolean;
    movieLocationsIsFetched: boolean;
    movieLocationsIsFetching: boolean;
}

export interface IMovieLocation {
    movie: IMovie;
    id: string;
    marker: IMarker;
}

export interface IMapDataState {
    locations: IMovieLocation[];
}

export interface IMapDataDispatchProps {
    /**
     * Emitted when the app is loaded for the first time.
     */
    fetchMoviesData: () => void;

    /**
     * Emitted for fetching all movies locations.
     */
    fetchMovieLocations: (movies: { [id: string]: IMovie }) => void;
}
