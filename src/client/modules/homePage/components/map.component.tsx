import { Paper } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Refresh as RefreshIcon } from '@material-ui/icons';
import * as React from 'react';
import { GoogleMap, withGoogleMap, withScriptjs } from 'react-google-maps';
import { FormattedMessage } from 'react-intl';
import { compose, withHandlers, withProps } from 'recompose';
import { LocaleContext } from '../../../../client/contexts';
import { Lang } from '../../../Lang';
import { mapStyle } from './map-options';
import { IMapDataProps, IMapDataState, IMovieLocation } from './map.props.interface';
import { MapMarker } from './marker/map-marker.component';
import { MovieSearchContainer } from './movie-search/movie-search.container';

// tslint:disable:no-var-requires
const { MarkerClusterer } = require('react-google-maps/lib/components/addons/MarkerClusterer');
// tslint:enable:no-var-requires

const progressStyles = { width: '22px', height: '22px', marginRight: '7px', marginLeft: '7px' };

const mapOptions = {
    panControl: false,
    mapTypeControl: true,
    scrollwheel: true,
    styles: mapStyle
};

const MapComponent = compose(
    withProps({
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div style={{ height: `87%` }} />,
        mapElement: <div style={{ height: `100%` }} />
    }),
    withHandlers({
        onMarkerClustererClick: () => (markerClusterer) => {
            // const clickedMarkers = markerClusterer.getMarkers();
            // tslint:disable-next-line:no-console
            console.log(markerClusterer);
        }
    }),
    withScriptjs,
    withGoogleMap
)((props) => {
    return (
        <GoogleMap defaultZoom={11} options={mapOptions} defaultCenter={{ lat: 37.773972, lng: -122.431297 }}>
            <MarkerClusterer onClick={props.onMarkerClustererClick} averageCenter enableRetinaIcons gridSize={60}>
                {props.markers.map((location) => {
                    return <MapMarker lat={location.marker.lat} lng={location.marker.lng} id={location.id} key={location.id} movie={location.movie} />;
                })}
            </MarkerClusterer>
        </GoogleMap>
    );
});

export class Map extends React.Component<IMapDataProps, IMapDataState> {
    public componentWillMount() {
        this.setState({ locations: [] });
    }

    public componentDidMount() {
        this.props.fetchMoviesData();
    }

    public componentDidUpdate(prevProps: IMapDataProps) {
        const { moviesIsFetched, movies, movieLocationsIsFetched, movieLocations } = this.props;
        if (prevProps.moviesIsFetched !== moviesIsFetched && moviesIsFetched) {
            // This won't be fired until the movies data is fetched.
            this.props.fetchMovieLocations(movies);
        }

        if (prevProps.movieLocationsIsFetched !== movieLocationsIsFetched) {
            const mLocations: IMovieLocation[] = [];
            Object.keys(movieLocations).forEach((id) => {
                movieLocations[id].marker.forEach((m, i) => {
                    mLocations.push({
                        movie: movies[id],
                        id: `${id}-${i}`,
                        marker: {
                            lat: m.lat,
                            lng: m.lng
                        }
                    });
                });
            });

            this.setState({
                locations: mLocations
            });
        }
    }

    public render() {
        const { classes, movieLocationsIsFetching } = this.props;
        return (
            <LocaleContext.Consumer>
                {({ localeType }) => (
                    <Paper className={classes.root}>
                        <FormattedMessage id={movieLocationsIsFetching ? Lang.LOADING : Lang.REFRESH}>
                            {(msg) => (
                                <>
                                    <Button
                                        style={{ marginTop: '20px' }}
                                        variant="contained"
                                        color="primary"
                                        disabled={movieLocationsIsFetching}
                                        onClick={this.props.fetchMoviesData}
                                    >
                                        {movieLocationsIsFetching && (
                                            <>
                                                <CircularProgress style={progressStyles} />
                                                {msg}
                                            </>
                                        )}
                                        {!movieLocationsIsFetching && (
                                            <>
                                                <RefreshIcon style={{ marginLeft: '7px', marginRight: '7px' }} />
                                                {msg}
                                            </>
                                        )}
                                    </Button>
                                    <MovieSearchContainer />
                                </>
                            )}
                        </FormattedMessage>

                        <MapComponent
                            googleMapURL={`https://maps.googleapis.com/maps/api/js?key=AIzaSyCnwhP_P6zvZPzg5h8rP_1IzSoXASVUsEI&v=weekly&libraries=places&language=${localeType}`}
                            key="map"
                            markers={this.state.locations}
                        />
                    </Paper>
                )}
            </LocaleContext.Consumer>
        );
    }
}
