import gql from 'graphql-tag';

/**
 * All Movies queries
 */
export const MapQueries = {
    gql: {
        movie: gql`
            query {
                movie {
                    actor_1
                    actor_2
                    actor_3
                    director
                    locations
                    production_company
                    release_year
                    title
                    writer
                }
            }
        `
    }
};
