import { bindActionCreators, Dispatch } from 'redux';
import { IMovieModelLocation } from '../reducers/movie-location.interface';
import { IMovie } from '../reducers/movie.interface';
import { MapDataActionType } from './map.actions.type';
import { MovieDataActionType } from './movie.actions.type';

const OPEN_CAGE_DATA_URL = 'https://api.opencagedata.com/geocode/v1/json?key=617e732098ee43bf988633477a21edbb&q=';
const SF_FILM_LOCATIONS_URL = 'https://data.sfgov.org/resource/wwmu-gmzc.json';

const MovieDataActionDispatch = {
    fetchMoviesData: () => async (dispatch) => {
        dispatch({ type: MovieDataActionType.FETCHING_DATA });
        dispatch({ type: MapDataActionType.RESET_DATA });

        try {
            const res = await fetch(encodeURI(`${SF_FILM_LOCATIONS_URL}`));
            const json = await res.json();
            return dispatch({ type: MovieDataActionType.FETCHED_DATA, payload: json });
        } catch (err) {
            return dispatch({ type: MovieDataActionType.FETCHED_DATA_FAILURE, payload: err });
        }
    },

    fetchMovieLocations: (movies: { [id: string]: IMovie }) => async (dispatch) => {
        dispatch({ type: MapDataActionType.RESET_DATA });
        dispatch({ type: MapDataActionType.FETCHING_DATA });
        const locations: IMovieModelLocation = {};
        if (movies) {
            Object.keys(movies).forEach((id) => {
                const movie = movies[id];
                if (movie.locations) {
                    locations[id] = {
                        locations: movie.locations,
                        marker: []
                    };
                }
            });

            const keys = Object.keys(locations);
            for (let i = 0; i < keys.length; i++) {
                if (i > 15) {
                    break;
                }
                try {
                    const res = await fetch(encodeURI(`${OPEN_CAGE_DATA_URL}${locations[keys[i]].locations}, San Francisco, California, USA`));
                    const json = await res.json();
                    if (json && json.results && json.results.length > 0) {
                        json.results.forEach((result) => {
                            locations[keys[i]].marker.push({
                                lat: result.geometry.lat,
                                lng: result.geometry.lng
                            });
                        });
                    }
                } catch {
                    // Nothing to do
                }
            }
            return dispatch({ type: MapDataActionType.FETCHED_DATA, payload: locations });
        }
        return dispatch({ type: MapDataActionType.FETCHED_DATA, payload: [] });
    }
};

export const MovieDataActions = (dispatch: Dispatch) => bindActionCreators({ ...MovieDataActionDispatch }, dispatch);
