const PREFIX = 'MAP_';

export const MapDataActionType = {
    RESET_DATA: `${PREFIX}RESET_DATA`,
    FETCHING_DATA: `${PREFIX}FETCHING_DATA`,
    FETCHED_DATA: `${PREFIX}FETCHED_DATA`
};
