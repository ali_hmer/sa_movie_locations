import { IMovie } from './movie.interface';

export interface IMovieStore {
    readonly isFetching: boolean;
    readonly isFetched: boolean;
    readonly movies: {
        [id: string]: IMovie;
    };
}
