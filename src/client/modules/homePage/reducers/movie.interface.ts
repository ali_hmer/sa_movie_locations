export interface IMovie {
    actor_1: string;
    actor_2: string;
    actor_3: string;
    director: string;
    locations: string;
    production_company: string;
    release_year: string;
    title: string;
    writer: string;
}
