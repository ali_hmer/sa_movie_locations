export interface IMarker {
    lat: number;
    lng: number;
}

export interface IMovieModelLocation {
    [id: string]: { marker: IMarker[]; locations: string };
}
