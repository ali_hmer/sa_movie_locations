import { Action, handleActions } from 'redux-actions';
import { MapDataActionType } from '../actions/map.actions.type';
import { IMovieModelLocation } from './movie-location.interface';
import { IMovieLocationStore } from './movie-locations.store.interface';

const initialState = {
    isFetching: false,
    isFetched: false,
    movieLocations: {}
} as IMovieLocationStore;

const fetchingMovieLocationData = (state: IMovieLocationStore): IMovieLocationStore => ({ ...state, isFetching: true });

const fetchingMovieLocationDataSuccess = (state: IMovieLocationStore, { payload }: Action<IMovieModelLocation>): IMovieLocationStore => {
    const movieLocations = {};
    if (payload) {
        Object.keys(payload).forEach((item) => {
            movieLocations[item] = payload[item];
        });
    }
    return {
        ...state,
        isFetching: false,
        isFetched: true,
        movieLocations
    };
};

const resetMovieData = (): IMovieLocationStore => initialState;

export const MovieLocationReducer = handleActions<IMovieLocationStore, any>(
    {
        [MapDataActionType.FETCHING_DATA]: fetchingMovieLocationData,
        [MapDataActionType.FETCHED_DATA]: fetchingMovieLocationDataSuccess,
        [MapDataActionType.RESET_DATA]: resetMovieData
    },
    initialState
);
