import { Action, handleActions } from 'redux-actions';
import * as UUID from 'uuid';
import { MovieDataActionType } from '../actions/movie.actions.type';
import { IMovie } from './movie.interface';
import { IMovieStore } from './movie.store.interface';

const initialState = {
    isFetching: false,
    isFetched: false,
    movies: {}
} as IMovieStore;

/**
 * Returns a new Globally Unique Identifier (Guid).
 */
const newGuid = (): string => {
    return UUID.v4();
};

const fetchingMovieData = (state: IMovieStore): IMovieStore => ({ ...state, isFetching: true, isFetched: false });

const fetchingMovieDataSuccess = (state: IMovieStore, { payload }: Action<IMovie[]>): IMovieStore => {
    let moviesCollection: {
        [id: string]: IMovie;
    } = {};
    if (payload) {
        const flags = {};
        const uniqueMovies: IMovie[] = [];
        for (const entry of payload) {
            if (!flags[entry.title]) {
                flags[entry.title] = true;
                uniqueMovies.push(entry);
            }
        }
        uniqueMovies.forEach((item) => {
            moviesCollection = { ...moviesCollection, ...{ [newGuid()]: { ...item } } };
        });
    }

    return {
        ...state,
        isFetching: false,
        isFetched: true,
        movies: moviesCollection
    };
};

const fetchingMovieDataFailure = (state: IMovieStore): IMovieStore => ({ ...state, isFetching: false, isFetched: false });

export const MovieReducer = handleActions<IMovieStore, any>(
    {
        [MovieDataActionType.FETCHING_DATA]: fetchingMovieData,
        [MovieDataActionType.FETCHED_DATA]: fetchingMovieDataSuccess,
        [MovieDataActionType.FETCHED_DATA_FAILURE]: fetchingMovieDataFailure
    },
    initialState
);
