import { IMovieModelLocation } from './movie-location.interface';

export interface IMovieLocationStore {
    readonly isFetching: boolean;
    readonly isFetched: boolean;
    readonly movieLocations: IMovieModelLocation;
}
