import { DocumentProps, NextDocumentContext } from 'next/document';
import * as React from 'react';
import { addLocaleData, InjectedIntlProps, injectIntl, IntlProvider } from 'react-intl';
import { LocaleContext, LocaleContextType, LocaleType, localeTypeFromStorage, saveLocaleType } from '../contexts';

declare const window: any;

// Register React Intl's locale data for the user's locale in the browser. This
// locale data was added to the page by `pages/_document.js`. This only happens
// once, on initial page load in the browser.
if (typeof window !== 'undefined' && window.ReactIntlLocaleData) {
    Object.keys(window.ReactIntlLocaleData).forEach((lang) => {
        addLocaleData(window.ReactIntlLocaleData[lang]);
    });
}

interface OwnProps {
    locale: any;
    messages: any;
    now: any;
}

interface Props {
    getInitialProps: (ctx: NextDocumentContext) => DocumentProps;
}

interface State extends LocaleContextType {
    localeType: LocaleType;
    toggleLocale: () => void;
}

export const withIntl = (Page: React.ComponentType<OwnProps & InjectedIntlProps> & Props) => {
    // & { getInitialProps(ctx: NextDocumentContext): DocumentProps }
    const IntlPage = injectIntl<OwnProps>(Page);

    return class PageWithIntl extends React.Component<OwnProps, State> {
        constructor(props: OwnProps, context: any) {
            super(props, context);
            const lt: LocaleType = props.locale || localeTypeFromStorage;
            this.state = {
                localeType: lt,
                toggleLocale: this.toggleLocale
            };
        }

        static async getInitialProps(contextApp: any) {
            const props = Page && Page.getInitialProps ? await Page.getInitialProps(contextApp) : {};

            // Get the `locale` and `messages` from the request object on the server.
            // In the browser, use the same values that the server serialized.
            const { locale, messages } = (contextApp.ctx.req as any) || window.__NEXT_DATA__.props.initialProps;

            // Always update the current time on page load/transition because the
            // <IntlProvider> will be a new instance even with pushState routing.
            const now = Date.now();

            return { ...props, locale, messages, now };
        }

        componentDidMount() {
            const locale = this.props.locale || 'en';
            document.cookie = `locale=${locale};path=/;max-age=2147483647`;
        }

        toggleLocale = () => {
            this.setState((prevState) => {
                const prevLocale = prevState.localeType;

                const newLocaleType: LocaleType = prevLocale === 'ar' ? 'en' : 'ar';

                saveLocaleType(newLocaleType);
                const oldDir = prevState.localeType === 'ar' ? 'rtl' : 'ltr';
                const newDir = newLocaleType === 'ar' ? 'rtl' : 'ltr';

                document.cookie = `locale=${newLocaleType};path=/;max-age=2147483647`;

                if (oldDir !== newDir) {
                    setTimeout(() => {
                        window.location.reload();
                    }, 1000);
                }

                return { ...prevState, localeTypeFromStorage: newLocaleType };
            });
        };

        render() {
            const { locale, messages, now } = this.props;
            return (
                <IntlProvider locale={locale} messages={messages} initialNow={now}>
                    <LocaleContext.Provider value={this.state}>
                        <IntlPage {...this.props} />
                    </LocaleContext.Provider>
                </IntlProvider>
            );
        }
    };
};
