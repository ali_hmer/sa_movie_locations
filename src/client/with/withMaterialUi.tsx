import { CssBaseline, MuiThemeProvider, PaletteType } from '@material-ui/core';
import * as cookie from 'cookie';
import * as React from 'react';
import { paletteTypeFromStorage, savePaletteType, ThemeContext, ThemeContextType } from '../contexts';
import { AppFrame } from '../modules/common/components';
import { PageContext, StylesContext } from '../styles/StylesContext';

// tslint:disable-next-line
const JssProvider = require('react-jss/lib/JssProvider').default;

export const withMaterialUi = (BaseComponent: React.ComponentClass & { getInitialProps?(ctx: any): Promise<any> }) => {
    interface Props {
        intl: any;
        paletteType: PaletteType;
    }

    interface State extends Pick<ThemeContextType, 'paletteType' | 'toggleTheme'> {
        readonly pageContext: PageContext;
        readonly isRtl: boolean;
    }

    class Component extends React.Component<Props, State> {
        constructor(props: Props, context: any) {
            super(props, context);

            const isRtl = props.intl.locale === 'ar';
            this.state = {
                paletteType: props.paletteType,
                pageContext: StylesContext.getPageContext(props.paletteType, isRtl),
                toggleTheme: this.toggleTheme,
                isRtl
            };
        }

        static async getInitialProps(contextApp: any) {
            // const { paletteType as pt } = (ctx.req as any); // || window.__NEXT_DATA__.props.initialProps;
            const { req } = contextApp.ctx;
            const headers = req && req.headers;
            const cookies = cookie.parse((headers && headers.cookie) || '');
            const pt = cookies.paletteType || paletteTypeFromStorage();

            if (typeof BaseComponent.getInitialProps === 'function') {
                const baseObject = await BaseComponent.getInitialProps(contextApp);
                return {
                    ...baseObject,
                    paletteType: pt
                };
            }

            return {
                paletteType: pt
            };
        }

        toggleTheme = () => {
            this.setState((prevState) => {
                const appliedPaletteType = prevState.paletteType;
                const newPaletteType: PaletteType = appliedPaletteType === 'light' ? 'dark' : 'light';
                savePaletteType(newPaletteType);
                document.cookie = `paletteType=${newPaletteType};path=/;max-age=2147483647`;

                StylesContext.setGlobalMaterialUI(newPaletteType, prevState.isRtl);

                return { ...prevState, paletteType: newPaletteType, pageContext: StylesContext.getPageContext(newPaletteType, prevState.isRtl) };
            });
        };

        componentDidMount() {
            // Remove the server-side injected CSS.
            // const pt = this.props.paletteType;
            // document.cookie = `paletteType=${pt};path=/;max-age=2147483647`;
            const jssStyles = document.querySelector('#jss-server-side');
            if (jssStyles && jssStyles.parentNode) {
                jssStyles.parentNode.removeChild(jssStyles);
            }
        }

        render() {
            const {
                pageContext: { jss, sheetsRegistry, generateClassName, sheetsManager, theme }
            } = this.state;
            return (
                <JssProvider jss={jss} registry={sheetsRegistry} generateClassName={generateClassName}>
                    <MuiThemeProvider theme={theme} sheetsManager={sheetsManager}>
                        <CssBaseline />
                        <AppFrame>
                            <ThemeContext.Provider
                                value={{
                                    paletteType: this.state.paletteType,
                                    toggleTheme: this.state.toggleTheme
                                }}
                            >
                                <BaseComponent {...this.props} />
                            </ThemeContext.Provider>
                        </AppFrame>
                    </MuiThemeProvider>
                </JssProvider>
            );
        }
    }

    return Component;
};
