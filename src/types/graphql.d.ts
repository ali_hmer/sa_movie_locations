/* tslint:disable */
import { GraphQLResolveInfo } from 'graphql';

export type Resolver<Result, Parent = any, Context = any, Args = any> = (
    parent: Parent,
    args: Args,
    context: Context,
    info: GraphQLResolveInfo,
) => Promise<Result> | Result;

export type SubscriptionResolver<Result, Parent = any, Context = any, Args = any> = {
    subscribe<R = Result, P = Parent>(parent: P, args: Args, context: Context, info: GraphQLResolveInfo): AsyncIterator<R | Result>;
    resolve?<R = Result, P = Parent>(parent: P, args: Args, context: Context, info: GraphQLResolveInfo): R | Result | Promise<R | Result>;
};

export interface Query {
    me?: Account | null;
    about?: About | null;
}

export interface Account {
    id: string;
    firstName: string;
    lastName: string;
}

export interface About {
    id: string;
    ocname: string;
    title: string;
    content: string;
}

export interface Mutation {
    updateMe?: Account | null;
}

export interface MeUpdateInput {
    id: string;
    firstName?: string | null;
    lastName?: string | null;
}
export interface UpdateMeMutationArgs {
    input?: MeUpdateInput | null;
}

export namespace QueryResolvers {
    export interface Resolvers<Context = any> {
        me?: MeResolver<Account | null, any, Context>;
    }

    export type MeResolver<R = Account | null, Parent = any, Context = any> = Resolver<R, Parent, Context>;
}

export namespace AccountResolvers {
    export interface Resolvers<Context = any> {
        id?: IdResolver<string, any, Context>;
        firstName?: FirstNameResolver<string, any, Context>;
        lastName?: LastNameResolver<string, any, Context>;
    }

    export type IdResolver<R = string, Parent = any, Context = any> = Resolver<R, Parent, Context>;
    export type FirstNameResolver<R = string, Parent = any, Context = any> = Resolver<R, Parent, Context>;
    export type LastNameResolver<R = string, Parent = any, Context = any> = Resolver<R, Parent, Context>;
}


export namespace MutationResolvers {
    export interface Resolvers<Context = any> {
        updateMe?: UpdateMeResolver<Account | null, any, Context>;
    }

    export type UpdateMeResolver<R = Account | null, Parent = any, Context = any> = Resolver<R, Parent, Context, UpdateMeArgs>;
    export interface UpdateMeArgs {
        input?: MeUpdateInput | null;
    }
}
